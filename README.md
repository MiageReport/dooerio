<img src="https://gitlab.com/MiageReport/dooerio/raw/master/resources/img/dooer.png">
# COE's dashboard

## Description

Le **dashboard du CEO** est un projet d’informatique décisionnel. Il doit permettre de **visualiser rapidement quelques indicateurs clés**.

## Indicateurs

* Courbe d’évolution du nombre de missions proposées
* Taux d’acceptation des proposition de réalisation mission
* Délais moyen d’édition de facture

## Prestataire

<img src="https://gitlab.com/MiageReport/dooerio/raw/master/resources/img/mr.png" width="150px">


**Chef de projet**: Aurélien ROUSSEAU

**Expert dataviz**: Cyril FERLICOT

**Analyste BI**: Maxime PIQUET



# Flux du projet

<img src="https://gitlab.com/MiageReport/dooerio/raw/9b0dcfbc3747d0cf545ac678a8d6d0a5fe8df602/resources/img/20170218_MR_SchemaBI.png">

### 1. Récupération des données externes ###

Nous faisons l'extraction des données des projets _Factures_ et _Missions_ via l'ETL Talend.
Nous chargeons ensuite les données dans notre DataWarehouse (DWH).

Très peu de transformations sont faites durant cette étape ( _1 pour 1_ ).

### 2. Transformation de la donnée ###

Nous utilisons Talend pour extraire les données du DWH, les transformer, puis pour les charger dans notre DataMart (DM).

### 3. Restitution de la donnée ###

Nous utilisons Metabase pour bénéficier d'un outil de visualisation puissant.

Metabase va analyser les données du DM pour proposer un ensemble de critères afin de définir des règles très facilement.
Nous avons donc la possibilité de visualiser de la donnée sans devoir posséder des connaissances en développement ou requêtage BDD.