--
-- Datawarehouse
--


-- DROP SCHEMA dwh;

CREATE SCHEMA dwh AUTHORIZATION cepcwzmuwhoqgx;

--
-- Dimensions tables (look up)
--

-- DROP TABLE dwh.d_calendar;

CREATE TABLE dwh.d_calendar
(
	sid_calendar uuid NOT NULL,
	day integer NOT NULL,
	week integer NOT NULL,
	month integer NOT NULL,
	year integer NOT NULL
);

ALTER TABLE dwh.d_calendar
	ADD CONSTRAINT pk_d_calendar PRIMARY KEY (sid_calendar),
	ADD CONSTRAINT uq_d_calendar UNIQUE (day, week, month, year);

-- DROP TABLE dwh.d_customers;

CREATE TABLE dwh.d_customers
(
	sid_customer uuid NOT NULL,
	id uuid NOT NULL,
	first_name text,
	last_name text,
	email text
);

ALTER TABLE dwh.d_customers
	ADD CONSTRAINT pk_d_customers PRIMARY KEY (sid_customer),
	ADD CONSTRAINT uq_d_customers UNIQUE (id);


-- DROP TABLE dwh.d_undertakers;

CREATE TABLE dwh.d_undertakers
(
	sid_undertaker uuid NOT NULL,
	id uuid NOT NULL,
	first_name text,
	last_name text,
	email text
);

ALTER TABLE dwh.d_undertakers
	ADD CONSTRAINT pk_d_undertakers PRIMARY KEY (sid_undertaker),
	ADD CONSTRAINT uq_d_undertakers UNIQUE (id);

--
-- Fact tables
--

-- DROP TABLE dwh.f_invoice;

CREATE TABLE dwh.f_invoice
(
	sid_d_calendar_start uuid NOT NULL,
	sid_d_calendar_end uuid NOT NULL,
	sid_d_customer uuid NOT NULL,
	sid_d_undertaker uuid NOT NULL,
	edition_delay integer NOT NULL
);

ALTER TABLE dwh.f_invoice
	ADD CONSTRAINT pk_f_invoice PRIMARY KEY (sid_d_calendar_start, sid_d_calendar_end, sid_d_customer, sid_d_undertaker);


-- DROP TABLE dwh.f_proposition;

CREATE TABLE dwh.f_proposition
(
	sid_d_calendar uuid NOT NUll,
	sid_d_customer uuid NOT NULL,
	number_of_proposition integer NOT NULL
);

ALTER TABLE dwh.f_proposition
	ADD CONSTRAINT pk_f_proposition PRIMARY KEY (sid_d_calendar, sid_d_customer);


-- DROP TABLE dwh.f_acceptance;

CREATE TABLE dwh.f_acceptance
(
	sid_d_calendar uuid NOT NULL,
	sid_d_customer uuid NOT NULL,
	sid_d_undertaker uuid NOT NULL,
	rate_of_acceptance NUMERIC(3,1) NOT NULL
);

ALTER TABLE dwh.f_acceptance
	ADD	CONSTRAINT pk_f_acceptance PRIMARY KEY (sid_d_calendar, sid_d_customer, sid_d_undertaker);


--
-- Datamart
--

-- DROP SCHEMA dm;

CREATE SCHEMA dm AUTHORIZATION cepcwzmuwhoqgx;

--
-- Dimensions tables (look up)
--

-- DROP TABLE dm.d_calendar;

CREATE TABLE dm.d_calendar
(
	sid_calendar uuid NOT NULL,
	day integer NOT NULL,
	week integer NOT NULL,
	month integer NOT NULL,
	year integer NOT NULL
);

ALTER TABLE dm.d_calendar
	ADD	CONSTRAINT pk_d_calendar PRIMARY KEY (sid_calendar),
	ADD CONSTRAINT uq_d_calendar UNIQUE (day, week, month, year);



-- DROP TABLE dm.d_customers;

CREATE TABLE dm.d_customers
(
	sid_customer uuid NOT NULL,
	id uuid NOT NULL,
	first_name text,
	last_name text,
	email text
);

ALTER TABLE dm.d_customers
	ADD CONSTRAINT pk_d_customers PRIMARY KEY (sid_customer),
	ADD CONSTRAINT uq_d_customers UNIQUE (id);



-- DROP TABLE dm.d_undertakers;

CREATE TABLE dm.d_undertakers
(
	sid_undertaker uuid NOT NULL,
	id uuid NOT NULL,
	first_name text,
	last_name text,
	email text
);

ALTER TABLE dm.d_undertakers
	ADD CONSTRAINT pk_d_undertakers PRIMARY KEY (sid_undertaker),
	ADD CONSTRAINT uq_d_undertakers UNIQUE (id);


--
-- Fact tables
--

-- DROP TABLE dm.f_invoice;

CREATE TABLE dm.f_invoice
(
	sid_d_calendar_start uuid NOT NULL,
	sid_d_calendar_end uuid NOT NULL,
	sid_d_customer uuid NOT NULL,
	sid_d_undertaker uuid NOT NULL,
	edition_delay integer NOT NULL
);

ALTER TABLE dm.f_invoice
	ADD CONSTRAINT pk_f_invoice PRIMARY KEY (sid_d_calendar_start, sid_d_calendar_end, sid_d_customer, sid_d_undertaker);


-- DROP TABLE dm.f_proposition;

CREATE TABLE dm.f_proposition
(
	sid_d_calendar uuid NOT NUll,
	sid_d_customer uuid NOT NULL,
	number_of_proposition integer NOT NULL
);

ALTER TABLE dm.f_proposition
	ADD CONSTRAINT pk_f_proposition PRIMARY KEY (sid_d_calendar, sid_d_customer);

-- DROP TABLE dm.f_acceptance;

CREATE TABLE dm.f_acceptance
(
	sid_d_calendar uuid NOT NULL,
	sid_d_customer uuid NOT NULL,
	sid_d_undertaker uuid NOT NULL,
	rate_of_acceptance  NUMERIC(3,1)
);

ALTER TABLE dm.f_acceptance
ADD CONSTRAINT pk_f_acceptance PRIMARY KEY (sid_d_calendar, sid_d_customer, sid_d_undertaker);
